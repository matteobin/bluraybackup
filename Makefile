BIN     = bluraybackup
VERSION = 2.0

PREFIX = /usr/local

CPPFLAGS_BLURAYBACKUP = -DBIN=\"${BIN}\" -DVERSION=\"${VERSION}\" ${CPPFLAGS}
OPTIMIZATION          = -Os
CFLAGS_BLURAYBACKUP   = ${OPTIMIZATION} -ansi -pedantic ${CPPFLAGS_BLURAYBACKUP} ${CFLAGS}
LDFLAGS_BLURAYBACKUP  = -lbluray ${LDFLAGS}

SRC = ${BIN}.c
OBJ = ${SRC:.c=.o}

all: ${BIN}

.c.o:
	${CC} ${CFLAGS_BLURAYBACKUP} -c $< -o $@
	
${BIN}: ${OBJ}
	${CC} -o ${BIN} ${OBJ} ${LDFLAGS_BLURAYBACKUP}

clean:
	rm -f ${BIN} ${BIN}-[1-9].[0-9].tar.gz *.o

debug:
	@make OPTIMIZATION=-ggdb

dist: clean
	mkdir ${BIN}-${VERSION}
	cp -R ${BIN}.1 ${SRC} LICENSE Makefile README ${BIN}-${VERSION}
	tar -cf ${BIN}-${VERSION}.tar ${BIN}-${VERSION}
	gzip ${BIN}-${VERSION}.tar
	rm -fr ${BIN}-${VERSION}

install: ${BIN}
	# bin
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ${BIN} ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/${BIN}
	# doc
	mkdir -p ${DESTDIR}${PREFIX}/share/doc/${BIN}
	cp -f README ${DESTDIR}${PREFIX}/share/doc/${BIN}
	chmod 644 ${DESTDIR}${PREFIX}/share/doc/${BIN}/README
	# man page
	mkdir -p ${DESTDIR}${PREFIX}/share/man/man1
	sed 's/VERSION/${VERSION}/' < ${BIN}.1 > ${DESTDIR}${PREFIX}/share/man/man1/${BIN}.1
	chmod 644 ${DESTDIR}${PREFIX}/share/man/man1/${BIN}.1

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/${BIN}
	rm -fr ${DESTDIR}${PREFIX}/share/doc/${BIN}
	rm -f ${DESTDIR}${PREFIX}/share/man/man1/${BIN}.1

.PHONY: all clean dist install uninstall
